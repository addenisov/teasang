import {Component, Input, OnInit} from '@angular/core';
import {card} from '../app.component';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
  interpolation: ['{{', '}}']
})


export class CardComponent implements OnInit{

  @Input() card: card;
  @Input() index: number;

  title = 'My card title';
  text = 'My text';
  cardDate: Date = new Date();
  number = 42;
  obj = {name: 'Artem', age: 20, job: 'student'};
  disabled = false;
  textColor: string;
  imUrl = 'https://sun9-23.userapi.com/c626122/u325866629/video/l_ccf2b1ce.jpg';


  fromInput(event: any): void {
  this.title = event.target.value;
  }


  changeTitle(): void {
    this.title = 'title has been changed!';
  }


  ngOnInit(): void {
    setTimeout(() => {
      this.imUrl = 'https://pbs.twimg.com/media/EWQ3OCdX0AIbRwn.png';
      this.disabled = true;
    }, 3000);

  }

  getInfo(): string {
    return 'this is my info';
  }

}
