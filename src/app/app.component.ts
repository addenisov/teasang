import { Component } from '@angular/core';

export interface card {
  title: string;
  text: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  view = true;

  cards: card[] = [
    {title: 'card 1', text: 'this is card number 1!'},
    {title: 'card 2', text: 'this is card number 2!'},
    {title: 'card 3', text: 'this is card number 3'}
  ];

  viewCards(): void {
    this.view = !this.view;
  }
}
