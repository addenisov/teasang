import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {Routes, RouterModule} from '@angular/router'

import { AppComponent } from './app.component';
import {CardComponent} from './card/Card.component';
import { FormComponent } from './form/form.component';
import { NewPageComponent } from './new-page/new-page.component';



const appRoutes: Routes = [
  { path: 'new', component: NewPageComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    CardComponent,
    FormComponent,
    NewPageComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
